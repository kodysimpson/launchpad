package me.kodysimpson.launchpad;

import me.kodysimpson.launchpad.listeners.FallDamageListener;
import me.kodysimpson.launchpad.listeners.PlayerMoveListener;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

public final class LaunchPad extends JavaPlugin {

    public ArrayList<Player> jumping_players = new ArrayList<>();

    @Override
    public void onEnable() {
        System.out.println("[LaunchPad] Starting Up...");

        //load config
        getConfig().options().copyDefaults();
        saveDefaultConfig();

        //Register listeners
        getServer().getPluginManager().registerEvents(new PlayerMoveListener(this), this);
        getServer().getPluginManager().registerEvents(new FallDamageListener(this), this);
    }

    @Override
    public void onDisable() {
        System.out.println("[LaunchPad] Shutting down...");
    }
}
