package me.kodysimpson.launchpad.listeners;

import me.kodysimpson.launchpad.LaunchPad;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class FallDamageListener implements Listener {

    LaunchPad plugin;

    public FallDamageListener(LaunchPad plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onFallDamage(EntityDamageEvent e){
        if (e.getCause().equals(EntityDamageEvent.DamageCause.FALL) && plugin.getConfig().getBoolean("disable-fall-damage")){
            if (plugin.jumping_players.contains(e.getEntity())){
                plugin.jumping_players.remove(e.getEntity());
                e.setCancelled(true);
            }
        }
    }

}
